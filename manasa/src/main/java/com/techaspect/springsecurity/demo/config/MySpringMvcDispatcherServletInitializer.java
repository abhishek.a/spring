package com.techaspect.springsecurity.demo.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MySpringMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}
	
	//getServletConfigurationClasses() is used instead of <servlet> used in web.xml
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {DemoAppConfig.class };
	}

	//getServletMappings() is used instead of <servlet-mapping> used in web.xml	
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}

}
